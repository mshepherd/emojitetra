package sondow.tetra.game;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * The direction that a falling piece is facing.
 */
public enum Direction {
    NORTH("N"), EAST("E"), SOUTH("S"), WEST("W");

    public final String code;

    Direction(String code) {
        this.code = code;
    }

    public static Direction fromInitial(String initial) throws NoSuchDirectionException {
        Stream<Direction> stream = Arrays.stream(Direction.values());
        Optional<Direction> opt = stream.filter(d -> initial.equals(d.code)).findFirst();
        if (!opt.isPresent()) {
            throw new NoSuchDirectionException(initial);
        }
        return opt.get();
    }
}
