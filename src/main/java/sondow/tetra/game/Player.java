package sondow.tetra.game;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import sondow.tetra.conf.GameConfig;
import sondow.tetra.conf.GameConfigFactory;
import sondow.tetra.conf.Time;
import sondow.tetra.conf.Tips;
import sondow.tetra.conf.TweetContent;
import sondow.tetra.io.AirtableDatabase;
import sondow.tetra.io.Converter;
import sondow.tetra.io.Database;
import sondow.tetra.io.EmojiSet;
import sondow.tetra.io.Outcome;
import sondow.tetra.io.Referee;
import sondow.tetra.io.TwitterPollMaker;
import twitter4j.Card;
import twitter4j.Status;
import twitter4j.StatusWithCard;
import static java.time.temporal.ChronoUnit.SECONDS;

/**
 * Executes a game turn from start to finish.
 */
public class Player {

    private GameConfig gameConfig;
    private Database database;
    private Random random;
    private TwitterPollMaker pollMaker;
    private String twitterHandle;
    private Time time;
    private Game game;

    public Player(Random random, Time time, Database database, TwitterPollMaker pollMaker) {
        init();
        this.random = random;
        this.time = time;
        this.database = database;
        this.pollMaker = pollMaker;
    }

    public Player() {
        init();
        this.random = new Random();
        this.time = new Time();
        this.database = new AirtableDatabase(gameConfig);
        this.pollMaker = new TwitterPollMaker(gameConfig.getTwitterConfig());
    }

    private void init() {
        this.gameConfig = new GameConfigFactory().configure();
        this.twitterHandle = gameConfig.getTwitterConfig().getUser();
    }

    public Outcome play() {
        String state = database.readGameState();
        System.out.println("Read game state from database: " + state);
        Converter converter = new Converter(random);
        if (state == null) {
            EmojiSet emojiSet = EmojiSet.pickOne(random);
            game = new Game(emojiSet, random);
            game.spawnPiece();
        } else if (state.contains("{")) {
            // Legacy format
            game = converter.makeGameFromJson(state);
        } else {
            game = converter.makeGameFromCompressedString(state);
        }
        Outcome outcome = readPreviousPollAndPostNewTweets(false);
        int maxRetries = 3;
        for (int i = 1; i <= maxRetries && outcome.getRetryDelaySeconds() >= 1; i++) {
            System.out.println("retry " + i + ", outcome.getRetryDelaySeconds(): " + outcome
                    .getRetryDelaySeconds());
            waitForPollEnd(outcome.getRetryDelaySeconds());
            // Sometimes Twitter takes way too long to process finishing a poll, and I don't want
            // to wait indefinitely for that opaque and unreliable process to finish.
            boolean giveUpAndDoItAnyway = (i >= maxRetries);
            outcome = readPreviousPollAndPostNewTweets(giveUpAndDoItAnyway);
        }
        List<Status> tweets = outcome.getTweets();
        if (tweets != null && tweets.size() >= 1) {
            Status lastTweet = tweets.get(tweets.size() - 1);
            game.setTweetId(lastTweet.getId());
            game.setThreadLength(game.getThreadLength() + tweets.size());
            if (game.isGameOver()) {
                database.deleteGameState();
            } else {
                String jsonFromGame = converter.makeJsonFromGame(game);
                String compressed = converter.makeCompressedStringFromGame(game);
                System.out.println("Writing game state to database: " + compressed +
                        " which in JSON is " + jsonFromGame);
                database.writeGameState(compressed);
            }
        }
        return outcome;
    }

    private Outcome readPreviousPollAndPostNewTweets(boolean giveUpAndDoItAnyway) {
        Outcome outcome = new Outcome();
        Integer turnLengthMinutes = gameConfig.getTurnLengthMinutes();
        Long tweetId = game.getTweetId();
        outcome.setPreviousGameString(game.toString()); // Mainly for unit testing
        StatusWithCard previous = pollMaker.readPreviousTweet(tweetId);
        if (previous == null || shouldAssessResultsNow(giveUpAndDoItAnyway, outcome, previous)) {
            List<Status> tweets = postTweets(turnLengthMinutes, pollMaker, previous);
            outcome.setTweets(tweets);
        }
        return outcome;
    }

    private boolean shouldAssessResultsNow(boolean giveUpAndDoItAnyway, Outcome
            outcome, StatusWithCard previousTweet) {

        Card poll = previousTweet.getCard();
        boolean countsAreFinal = poll.isCountsAreFinal();
        System.out.println("are poll counts final? " + countsAreFinal);
        boolean assessResultsNow;
        if (giveUpAndDoItAnyway || countsAreFinal) {
            assessResultsNow = true;
        } else {
            Referee referee = new Referee();
            if (referee.hasSuperMajority(poll)) {
                assessResultsNow = true;
            } else {
                long tweetId = previousTweet.getId();
                long pollTimeSecondsRemaining = calculatePollSecondsRemaining(poll);
                if (pollTimeSecondsRemaining > 60) {
                    throw new RuntimeException("Close race ends much later. secondsRemaining=" +
                            pollTimeSecondsRemaining + " tweetId=" + tweetId);
                }
                outcome.setRetryDelaySeconds(pollTimeSecondsRemaining + 5);
                assessResultsNow = false;
            }
        }
        return assessResultsNow;
    }

    private List<Status> postTweets(Integer turnLengthMinutes,
            TwitterPollMaker pollMaker, StatusWithCard previousGameTweet) {
        Referee referee = new Referee();
        Long inReplyToStatusIdForNewGameTweet = null;
        boolean timeToBreakThread = (game.getThreadLength() >= 50);
        if (previousGameTweet != null) {
            long previousGameTweetId = previousGameTweet.getId();
            inReplyToStatusIdForNewGameTweet = previousGameTweetId; // If continuing thread
            Move move = referee.determineElectedMove(previousGameTweet);
            if (move != null) {
                try {
                    game.doMove(move);
                } catch (GameOverException e) {
                    System.out.println("Game over with score " + game.getScore());
                }
            }

            if (timeToBreakThread) {
                // Start a new thread.

                // Three new tweets.

                // 1. newThreadIntroTweet: In reply to null, the start of the new thread should
                // be a tweet, possibly with a short message explaining that we're continuing a
                // game from another thread, and a link back to the last game tweet of the old
                // thread.
                // 2. newGameTweet: In reply to tweet 1, newThreadIntroTweet, make the
                // newGameTweet.
                // 3. oldThreadOutroTweet: In reply to the last game tweet of the old thread, a
                // tweet containing a link to tweet 2, newGameTweet.
                String previousGameTweetUrl = buildTweetUrl(previousGameTweetId);
                String newThreadIntroText = "Continuing game from thread… " + previousGameTweetUrl;
                Status newThreadIntroTweet = pollMaker.tweet(newThreadIntroText, null, null);
                inReplyToStatusIdForNewGameTweet = newThreadIntroTweet.getId();
            }
        }

        List<Status> newGameTweets = new ArrayList<>();
        Optional<String> lineClearingString = game.getLineClearingString();
        if (lineClearingString.isPresent()) {
            String text = lineClearingString.get();
            Status lineClearingTweet = pollMaker
                    .tweet(text, inReplyToStatusIdForNewGameTweet, null);
            newGameTweets.add(lineClearingTweet);
            inReplyToStatusIdForNewGameTweet = lineClearingTweet.getId();
        }

        String text = game.toString();
        Status newGameTweet;
        if (game.isGameOver()) {
            // Game over tweet has no poll
            newGameTweet = pollMaker.tweet(text, inReplyToStatusIdForNewGameTweet, null);
        } else {
            List<String> choices = Move.toPollChoices(game.listLegalMoves());
            newGameTweet = pollMaker.postPoll(turnLengthMinutes, text, choices,
                    inReplyToStatusIdForNewGameTweet);
        }
        newGameTweets.add(newGameTweet);

        if (previousGameTweet != null && timeToBreakThread) {
            String newGameTweetUrl = buildTweetUrl(newGameTweet.getId());
            pollMaker.tweet("Game continues in new thread… " + newGameTweetUrl, previousGameTweet
                    .getId(), null);
            game.setThreadLength(0);
        }

        Tips tips = new Tips();
        ZonedDateTime now = time.nowZonedDateTime();
        if (tips.isTimeForTipMessage(now, gameConfig.getTurnLengthMinutes())) {
            time.waitASec();
            TweetContent content = tips.getMessageForDayOfMonth(now.getDayOfMonth());
            pollMaker.tweet(content.getMessage(), null, content.getFile());
        }

        return newGameTweets;
    }

    private String buildTweetUrl(long id) {
        return "https://twitter.com/" + twitterHandle + "/status/" + id;
    }

    private long calculatePollSecondsRemaining(Card poll) {
        Instant endDatetimeUtc = poll.getEndDatetimeUtc();
        Instant now = Instant.now();
        long pollTimeSecondsRemaining = Math.max(0, now.until(endDatetimeUtc, SECONDS));
        System.out.println("now = " + now + ", endDatetimeUtc = " + endDatetimeUtc +
                ", pollTimeSecondsRemaining = " + pollTimeSecondsRemaining);
        return pollTimeSecondsRemaining;
    }

    private void waitForPollEnd(long secondsToWait) {
        try {
            System.out.println("Waiting " + secondsToWait + " seconds.");
            Thread.sleep(secondsToWait * 1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static void main(String[] args) {
        Player player = new Player();
        player.play();
    }
}
