package sondow.tetra.conf;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * To get these messages out regularly but without much repetition, the message of the day and
 * the time that message gets tweeted are selected by an algorithm based on the current date.
 * <p>
 * If the current date of the month has a corresponding message, that message is the message of the
 * day.
 * <p>
 * If the current date of the month does not have a corresponding message, subtract the total
 * number of messages from the current date. The result is the key for the message of the day.
 */
public class Tips {

    /**
     * Yes, technically this would be more elegant as a list, but I deliberately made it a map of
     * dates to strings so that I would remember that the ordered placement of each message to its
     * corresponding date number is very important, to avoid repetitions of similar messages near
     * each other, and to remind future me that adding a new message somewhere in the collection
     * should require some thought, considering what got tweeted in the past few days.
     */
    private Map<Integer, TweetContent> datesToMessages = new HashMap<>();

    public Tips() {

        put(1, "Game tip\n\nBefore you vote, check the side of the tweet to see " +
                "what shape the Next piece is. Think about how you want to use both the current " +
                "and the next piece together.");
        put(2, "This game is and always will be free, but if you want to tip the " +
                "implementer, you can do so at patreon.com/JoeSondow\n\nTips are never required " +
                "and always appreciated. Thank you very much!");
        put(3, "Game tip\n\nIf there are comments below one of my tweets, read " +
                "the comments (yes really) for strategy ideas before you vote.");
        put(4, "If you want to drop some money in the tip jar for the implementer" +
                " of this game but you don't want recurring patreon payments, you can use " +
                "paypal.me/JoeSondow or ko-fi.com/JoeSondow\n\nTips are never required but much" +
                " appreciated. Thank you!");
        put(5, "Game tip\n\nClearing multiple lines in one move earns more points" +
                " than clearing the same number of lines one at a time.");
        put(6, "This game is maintained by @JoeSondow who also tries to make Twitter less of a " +
                "nightmare with accounts like @EmojiAquarium @EmojiMeadow @PicardTips " +
                "@RikerGoogling @WorfEmail. You can support these projects if you want to:\n\n" +
                "patreon.com/JoeSondow");
        put(7, "Game tip\n\nThis game is one of those rare places on the internet" +
                " where it’s helpful to read the comments. Some players draw diagrams showing " +
                "clever potential uses of the current and next pieces.");
        put(8, "When addressing the @EmojiTetra player base, please make an effort to " +
                "use inclusive terms like “fam”, “folks”, “team”, “gang”, “crew”, “players”, " +
                "“people”, “y’all”, “buddies”, or “friends” rather than “boys”, “guys”, “dudes”, " +
                "“lads”, or “fellas”.");
        put(9, "Game tip\n\nThis game grid is smaller than what you’re probably " +
                "used to. Try to keep the top four rows clear enough to be able to rotate a 1x4 " +
                "piece.");
        put(10, "Game tip\n\nWhen choosing where to drop a piece, consider what " +
                "\"silhouette\" or \"skyline\" will result at the bottom. The safest situation " +
                "is one where there is a good place to put any of the possible shapes.");
        put(11, "Game tip\n\nYou can see all comments to all recent @EmojiTetra " +
                "polls by searching twitter with this link: " +
                "https://twitter.com/search?q=emojitetra%20-from%3Aemojitetra&f=tweets&vertical" +
                "=default&src=typd");
        put(12, "Game tip\n\nThis game is all text. You can copy, paste, and edit" +
                " the game board in a reply to illustrate to other players what strategy you " +
                "recommend.");
        put(13, "Player Code of Conduct v2:\n\n" +
                "Don’t threaten or harass other players, even in jest, even with images or " +
                "emojis, on Twitter or elsewhere.\n\n" +
                "Be respectful.\n\n" +
                "Make the comments more fun for other people, not more unpleasant.\n\n" +
                "This is a non-exhaustive list. I block whomever I choose.");
        put(14, "Game tip\n\nKeep your eye on the \"Next\" indicator on the side " +
                "of each game tweet.");
        put(15, "When drawing your recommendation with emojis, avoid 1️⃣ and 2️⃣ because " +
                "they don’t render properly on some devices. Try one of these emoji pairs " +
                "instead:\n\n" +
                "🥇🥈\n"+
                "🔴🔵");
        put(16, "Game tip\n\nThe most common order of moves for a given piece is " +
                "rotate, sideways movement, then down and plummet. If you're in agreement with " +
                "others about the piece's destination, this order is your best bet to avoid vote " +
                "splitting.");
        put(17, "We have a high energy group of players, which is great, but " +
                "I want to avoid toxic behavior in this community. If you see anything in the " +
                "comments that makes you uncomfortable, DM @EmojiTetra about it.");
        put(18,
                "FAQ: https://gitlab.com/JoeSondow/emojitetra/blob/master/README.md");
        put(19, "Game tip\n\nThe all-time high score is in my account bio.");
        put(20, "How Scoring Works\n" +
                "\n" +
                "Single line cleared +100 points\n" +
                "2 lines in one move +250 points\n" +
                "3 lines in one move +525 points\n" +
                "4 lines in one move +1000 points\n" +
                "\n" +
                "Full board clear +1000 points\n" +
                "\n" +
                "https://gitlab.com/JoeSondow/emojitetra/blob/master/README" +
                ".md#how-does-the-score-board-work");
        put(21, new TweetContent("Here's a gif showing the rotation rules for the shapes that" +
                " rotate.", new FileClerk().getFile("emojitetrarotations.gif")));
        put(22, "Game Tip\n\nThe current shape choosing algorithm is completely random. The " +
                "shapes that came recently have no bearing on which shapes to expect soon.");
        put(23, "Game Tip\n\nThe game usually ends as a result of a combination of greed for " +
                "points, hope for a 1x4 piece, and bad luck. Go for risky fancy 4-line clears " +
                "near the bottom, not the top.");
        put(24, "You can help support this game by retweeting its game polls to help grow the " +
                "player base. Retweets are much appreciated.");
        put(25, "Game tip\n" + "\n" +
                "If you need to build towers to avoid burying holes, try to build them against " +
                "the far left or right side, not in the middle.");
    }

    private void put(int index, String s) {
        put(index, new TweetContent(s, null));
    }

    private void put(int index, TweetContent t) {
        datesToMessages.put(index, t);
    }

    public TweetContent getMessageForDayOfMonth(int dayOfMonth) {
        assert dayOfMonth >= 1;
        TweetContent tweetContent = datesToMessages.get(dayOfMonth);
        if (tweetContent == null) {
            int messageCount = datesToMessages.size();
            tweetContent = datesToMessages.get(dayOfMonth % messageCount);
        }
        return tweetContent;
    }

    /**
     * Peak time is 7am to 3pm Pacific Time, which is 2pm to 10pm UTC, or 14 to 22 UTC military
     * time. For a target time between 14 and 22 inclusive, which span 9 hours, mod the date by 9
     * and add 14.
     *
     * @param dayOfMonth the date of the month, e.g. 12 for the 12th day of the current month
     * @return the 0-24 hour of the day at which to tweet the message of the day
     */
    int getHourFor(int dayOfMonth) {
        return (dayOfMonth % 9) + 14;
    }

    /**
     * Checks whether it's time to tweet a tip message based on the date of the month, the hour
     * of the day, and whether the current time is within the first x minutes of the hour, where
     * x is the number of minutes between game tweets.
     *
     * @param dateTime        the current date time
     * @param intervalMinutes the number of minutes between game tweets
     * @return true iff it's time for a tip tweet
     */
    public boolean isTimeForTipMessage(ZonedDateTime dateTime, int intervalMinutes) {
        int dayOfMonth = dateTime.getDayOfMonth();
        int hour = dateTime.getHour();
        return getHourFor(dayOfMonth) == hour && dateTime.getMinute() < intervalMinutes;
    }

    List<String> getAllMessages() {
        Collection<TweetContent> values = datesToMessages.values();
        return values.stream().map(TweetContent::getMessage).collect(Collectors.toList());
    }
}
