package sondow.tetra.game

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import sondow.tetra.conf.FileClerk
import sondow.tetra.conf.Time
import sondow.tetra.io.Database
import sondow.tetra.io.Outcome
import sondow.tetra.io.TwitterPollMaker
import spock.lang.Specification
import twitter4j.Card
import twitter4j.Choice
import twitter4j.Status
import twitter4j.StatusWithCard

import java.time.ZonedDateTime

class PlayerSpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    private void initEnvironment() {
        envVars.set('twitter_account', 'EmojiTetraGamma')
        envVars.set('EmojiTetraGamma_twitter4j_oauth_accessToken', 'ghostpowder')
        envVars.set('EmojiTetraGamma_twitter4j_oauth_accessTokenSecret', 'auntfrenchie')
        envVars.set('CRED_AWS_ACCESS_KEY', 'lemongiant')
        envVars.set('CRED_AWS_SECRET_KEY', 'truthfulmarionette')
        envVars.set('CRED_AIRTABLE_API_KEY', 'booyah')
        envVars.set('CRED_AIRTABLE_BASE', 'yeahboi')
    }

    private String initialTeeGame = '' +
            '◽◽👔👔👔◽◽\n' +
            '◽◽◽👔◽◽◽　Next\n' +
            '◽◽◽◽◽◽◽　👘\n' +
            '◽◽◽◽◽◽◽　👘\n' +
            '◽◽◽◽◽◽◽　👘\n' +
            '◽◽◽◽◽◽◽　👘\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽　Score\n' +
            '◽◽◽◽◽◽◽　0\n' +
            '◽◽◽◽◽◽◽\n' +
            '◽◽◽◽◽◽◽'

    private List<String> startingMoves = ['⬅️ Left', '➡️ Right', '🔄 Rotate', '⬇️ Down']
    private List<String> squareMoves = ['⬅️ Left', '➡️ Right', '⬇️ Down', '⏬ Plummet']
    private List<String> afterDownMoves = ['↔️ Left or Right', '🔄 Rotate', '⬇️ Down', '⏬ Plummet']

    def "should tweet without a poll when game ends"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 0),
                new Choice("➡️️ Right", 0),
                new Choice("⬇️ Stop", 30) // Super majority
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        Status tweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        1 * database.readGameState() >> '{"thm":"FRUIT","thmLen":57,"thr":23,"rows": ' +
                '".7-LL.5-.TOOJ3-T3.ZZJ-L.T3OO-LLSJ.SS-.SSJSSL-SSOO.LL-.TOOJ3-T3.ZZJ-.I4ZZ",' +
                '"cur":{"t":"J","c":3,"r":0,"d":"S"},"nx":"L","sc":1500,"id":47563}'
        1 * pollMaker.readPreviousTweet(47563L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 47563L
        1 * pollMaker.tweet('' +
                '◽◽🍏◽◽◽◽\n' +
                '🍉🍉🍏🍏🍏◽◽　Next\n' +
                '◽🍋🍇🍇🍏🍏🍏　🍉\n' +
                '🔔🔔🔔🔔🔔🔔🔔　🍉\n' +
                '🔔🇬 🇦 🇲 🇪🔔🔔　🍉🍉\n' +
                '🔔🔔🇴 🇻 🇪 🇷🔔\n' +
                '🔔🔔🔔🔔🔔🔔🔔\n' +
                '🍓🍓🍇🍇◽🍉🍉　Score\n' +
                '◽🍋🍇🍇🍏🍏🍏　1500\n' +
                '🍋🍋🍋◽🍎🍎🍏\n' +
                '◽🍑🍑🍑🍑🍎🍎', 47563L, null) >> tweet
        1 * tweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.deleteGameState()
        outcome.tweets == [tweet]
        0 * _._
    }

    def "should start new twitter thread when database starts empty"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        Status tweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        1 * database.readGameState() >> null
        1 * pollMaker.readPreviousTweet(null) >> null
        1 * pollMaker.postPoll(20, initialTeeGame, startingMoves, null) >> tweet
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T15:02:55Z')
        1 * time.waitASec()
        1 * pollMaker.tweet('Game tip\n\nThe all-time high score is in my account bio.', null, null)
        1 * database.writeGameState('9*.7-2*.7o0IyaN0T3n1')
        1 * tweet.getId() >> 1234L
        0 * _._
        outcome.tweets == [tweet]
    }

    def "should continue thread when database has game state, with super majority"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 0),
                new Choice("➡️️ Right", 0),
                new Choice("🔄 Rotate", 0),
                new Choice("⬇️ Down", 30) // Super majority
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String gameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽◽　◽🍎\n' +
                '◽◽◽◽◽◽◽　🍎🍎\n' +
                '◽◽◽🍏◽◽◽　🍎\n' +
                '◽◽◽🍏◽◽◽\n' +
                '◽◽🍏🍏◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽◽◽◽◽　100\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        Status tweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        1 * database.readGameState() >> '{"thm":"FRUIT","thmLen":57,"thr":31,' +
                '"rows":"9*.7-.7-.I4ZZ","cur":{"t":"J","c":3,"r":2,"d":"E","p":"dn"},' +
                '"nx":"Z","sc":100,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.postPoll(20, gameText, afterDownMoves, 1234567890L) >> tweet
        1 * tweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.writeGameState('9*.7-.7-.I4ZZf100Z1vmuE5J3d32')
        outcome.tweets == [tweet]
        0 * _._
    }

    def 'should start new thread after thread gets long'() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard oldGameTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 30), // Super majority
                new Choice("➡️️ Right", 0),
                new Choice("🔄 Rotate", 0),
                new Choice("⬇️ Down", 0)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        Status newGameTweet = Mock()
        String gameText = '' +
                '◽🍏◽◽◽◽◽\n' +
                '◽🍏🍏🍏◽◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🍉\n' +
                '◽◽◽🍋◽◽◽　🍉\n' +
                '🍉◽🍋🍋🍋🍇🍇　🍉🍉\n' +
                '🍉🍉🍓🍏◽🍓🍓\n' +
                '◽🍓🍓🍏🍓🍓🍉\n' +
                '🍓🍓🍇🍇◽🍉🍉　Score\n' +
                '◽🍋🍇🍇🍏🍏🍏　1500\n' +
                '🍋🍋🍋◽🍎🍎🍏\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        Status introTweet = Mock()
        Status outroTweet = Mock()
        File file = new FileClerk().getFile("emojitetrarotations.gif")

        when:
        player.play()

        then:
        1 * database.readGameState() >> '3*.7-.3T.3-L.T3OO-LLSJ.SS-.SSJSSL-SSOO.LL-.TOOJ3-T3.ZZJ' +
                '-.I4ZZf1500L3k98S0J3n50'
        1 * pollMaker.readPreviousTweet(166220L) >> oldGameTweet
        2 * oldGameTweet.getCard() >> previousPoll
        2 * oldGameTweet.getId() >> 987654321L
        1 * pollMaker.tweet('Continuing game from thread… https://twitter' +
                '.com/EmojiTetraGamma/status/987654321', null, null) >> introTweet
        1 * introTweet.getId() >> 898989898L
        1 * pollMaker.postPoll(20, gameText, startingMoves, 898989898L) >> newGameTweet
        1 * pollMaker.tweet('Game continues in new thread… https://twitter' +
                '.com/EmojiTetraGamma/status/87654', 987654321L, null) >> outroTweet
        2 * newGameTweet.getId() >> 87654L
        1 * time.waitASec()
        1 * pollMaker.tweet("Here's a gif showing the rotation rules for the shapes that rotate" +
                ".", null, file)
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-21T17:04:55Z')
        1 * database.writeGameState(
                '3*.7-.3T.3-L.T3OO-LLSJ.SS-.SSJSSL-SSOO.LL-.TOOJ3-T3.ZZJ-.I4ZZf1500L1vmuS0J2f1'
        )
        0 * _._
    }

    def 'should start new thread after length 50, with line clearing tweet starting new thread'() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard oldGameTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("↔️ Left or Right", 5),
                new Choice("🔄 Rotate", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80) // Super majority (over 70)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽◽　◽🍎\n' +
                '◽◽◽◽◽◽◽　🍎🍎\n' +
                '◽◽◽🍏◽◽◽　🍎\n' +
                '◽◽◽🍏◽◽◽\n' +
                '◽◽🍏🍏◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '🍎◽◽◽🍎◽◽　100\n' +
                '🍎🍎◽◽🍎🍎🍎\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽🍏◽◽◽\n' +
                '🍎◽◽🍏🍎◽◽\n' +
                '💩💩💩💩💩💩💩　+100\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        String newGameText = '' +
                '◽◽🍎🍎◽◽◽\n' +
                '◽◽◽🍎🍎◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🍋\n' +
                '◽◽◽◽◽◽◽　🍋🍋\n' +
                '◽◽◽◽◽◽◽　🍋\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽🍏◽◽◽　200\n' +
                '🍎◽◽🍏🍎◽◽\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        Status introTweet = Mock()
        Status outroTweet = Mock()
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"FRUIT","thmLen":57,"thr":50,' +
                '"rows":"8*.7-Z.3Z..-ZZ..Z3-.I4ZZ","cur":{"t":"J","c":3,"r":5,"d":"E","p":' +
                '"dn"},"nx":"Z","sc":100,"id":987654321}'
        1 * pollMaker.readPreviousTweet(987654321L) >> oldGameTweet
        2 * oldGameTweet.getCard() >> previousPoll
        2 * oldGameTweet.getId() >> 987654321L
        1 * pollMaker.tweet('Continuing game from thread… https://twitter' +
                '.com/EmojiTetraGamma/status/987654321', null, null) >> introTweet
        1 * introTweet.getId() >> 898989898L
        1 * pollMaker.tweet(rowClearingText, 898989898L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, startingMoves, 555666777L) >> newGameTweet
        1 * pollMaker.tweet('Game continues in new thread… https://twitter' +
                '.com/EmojiTetraGamma/status/87654', 987654321L, null) >> outroTweet
        2 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-21T11:04:55Z')
        1 * database.writeGameState('8*.7-.3J.3-Z..JZ..-.I4ZZf200T1vmuN0Z3n2')
        0 * _._
    }

    def 'should start new thread after length 51, with line clearing tweet starting new thread'() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard oldGameTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("↔️ Left or Right", 5),
                new Choice("🔄 Rotate", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80) // Super majority (over 70)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽◽　◽🍎\n' +
                '◽◽◽◽◽◽◽　🍎🍎\n' +
                '◽◽◽🍏◽◽◽　🍎\n' +
                '◽◽◽🍏◽◽◽\n' +
                '◽◽🍏🍏◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '🍎◽◽◽🍎◽◽　100\n' +
                '🍎🍎◽◽🍎🍎🍎\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽🍏◽◽◽\n' +
                '🍎◽◽🍏🍎◽◽\n' +
                '💩💩💩💩💩💩💩　+100\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        String newGameText = '' +
                '◽◽🍎🍎◽◽◽\n' +
                '◽◽◽🍎🍎◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🍋\n' +
                '◽◽◽◽◽◽◽　🍋🍋\n' +
                '◽◽◽◽◽◽◽　🍋\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽🍏◽◽◽　200\n' +
                '🍎◽◽🍏🍎◽◽\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        Status introTweet = Mock()
        Status outroTweet = Mock()
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"FRUIT","thmLen":57,"thr":51,' +
                '"rows":"8*.7-Z.3Z..-ZZ..Z3-.I4ZZ","cur":{"t":"J","c":3,"r":5,"d":"E","p":' +
                '"dn"},"nx":"Z","sc":100,"id":987654321}'
        1 * pollMaker.readPreviousTweet(987654321L) >> oldGameTweet
        2 * oldGameTweet.getCard() >> previousPoll
        2 * oldGameTweet.getId() >> 987654321L
        1 * pollMaker.tweet('Continuing game from thread… https://twitter' +
                '.com/EmojiTetraGamma/status/987654321', null, null) >> introTweet
        1 * introTweet.getId() >> 898989898L
        1 * pollMaker.tweet(rowClearingText, 898989898L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, startingMoves, 555666777L) >> newGameTweet
        1 * pollMaker.tweet('Game continues in new thread… https://twitter' +
                '.com/EmojiTetraGamma/status/87654', 987654321L, null) >> outroTweet
        2 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-21T11:04:55Z')
        1 * database.writeGameState('8*.7-.3J.3-Z..JZ..-.I4ZZf200T1vmuN0Z3n2')
        0 * _._
    }

    def 'should continue 49-tweet thread with line clearing tweet as 50 and game tweet as 51'() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard oldGameTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("↔️ Left or Right", 5),
                new Choice("🔄 Rotate", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80) // Super majority (over 70)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false

        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽◽　◽🍎\n' +
                '◽◽◽◽◽◽◽　🍎🍎\n' +
                '◽◽◽🍏◽◽◽　🍎\n' +
                '◽◽◽🍏◽◽◽\n' +
                '◽◽🍏🍏◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '🍎◽◽◽🍎◽◽　100\n' +
                '🍎🍎◽◽🍎🍎🍎\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽🍏◽◽◽\n' +
                '🍎◽◽🍏🍎◽◽\n' +
                '💩💩💩💩💩💩💩　+100\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        String newGameText = '' +
                '◽◽🍎🍎◽◽◽\n' +
                '◽◽◽🍎🍎◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🍋\n' +
                '◽◽◽◽◽◽◽　🍋🍋\n' +
                '◽◽◽◽◽◽◽　🍋\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽🍏◽◽◽　200\n' +
                '🍎◽◽🍏🍎◽◽\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"FRUIT","thmLen":57,"thr":49,' +
                '"rows":"8*.7-Z.3Z..-ZZ..Z3-.I4ZZ","cur":{"t":"J","c":3,"r":5,"d":"E","p":' +
                '"dn"},"nx":"Z","sc":100,"id":987654321}'
        1 * pollMaker.readPreviousTweet(987654321L) >> oldGameTweet
        2 * oldGameTweet.getCard() >> previousPoll
        1 * oldGameTweet.getId() >> 987654321L
        1 * pollMaker.tweet(rowClearingText, 987654321L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, startingMoves, 555666777L) >> newGameTweet
        1 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-21T11:04:55Z')
        1 * database.writeGameState('8*.7-.3J.3-Z..JZ..-.I4ZZf200T1vmuN0Z3n51')
        0 * _._
    }

    def "should clear one row and tweet row-clearing tweet followed by new game poll tweet"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("↔️ Left or Right", 5),
                new Choice("🔄 Rotate", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80) // Super majority (over 70)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽◽　◽🍎\n' +
                '◽◽◽◽◽◽◽　🍎🍎\n' +
                '◽◽◽🍏◽◽◽　🍎\n' +
                '◽◽◽🍏◽◽◽\n' +
                '◽◽🍏🍏◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '🍎◽◽◽🍎◽◽　100\n' +
                '🍎🍎◽◽🍎🍎🍎\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽🍏◽◽◽\n' +
                '🍎◽◽🍏🍎◽◽\n' +
                '💩💩💩💩💩💩💩　+100\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        String newGameText = '' +
                '◽◽🍎🍎◽◽◽\n' +
                '◽◽◽🍎🍎◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🍋\n' +
                '◽◽◽◽◽◽◽　🍋🍋\n' +
                '◽◽◽◽◽◽◽　🍋\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽🍏◽◽◽　200\n' +
                '🍎◽◽🍏🍎◽◽\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"FRUIT","thmLen":57,"thr":31,' +
                '"rows":"8*.7-Z.3Z..-ZZ..Z3-.I4ZZ","cur":{"t":"J","c":3,"r":5,"d":"E","p":' +
                '"dn"},"nx":"Z","sc":100,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.tweet(rowClearingText, 1234567890L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, startingMoves, 555666777L) >> newGameTweet
        1 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.writeGameState('8*.7-.3J.3-Z..JZ..-.I4ZZf200T1vmuN0Z3n33')
        outcome.tweets == [lineClearingTweet, newGameTweet]
        0 * _._
    }

    def "should clear two rows and tweet row-clearing tweet followed by new game poll tweet"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("⬅️ Left", 5), // Super majority
                new Choice("➡️️ Right", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🍇🍇\n' +
                '◽◽◽◽◽◽◽　🍇🍇\n' +
                '◽◽◽🍎◽◽◽\n' +
                '◽◽🍎🍎◽◽◽\n' +
                '◽◽🍎◽◽◽◽\n' +
                '◽◽◽◽◽🍇🍇　Score\n' +
                '🍎🍇◽◽🍎🍇🍇　1000\n' +
                '🍎🍎◽🍇🍎🍎🍎\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽🍎◽🍇🍇\n' +
                '💩💩💩💩💩💩💩　+125\n' +
                '💩💩💩💩💩💩💩　+125\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        String newGameText = '' +
                '◽◽◽🍇🍇◽◽\n' +
                '◽◽◽🍇🍇◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🍋\n' +
                '◽◽◽◽◽◽◽　🍋🍋\n' +
                '◽◽◽◽◽◽◽　🍋\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽◽◽◽◽　1250\n' +
                '◽◽◽🍎◽🍇🍇\n' +
                '◽🍑🍑🍑🍑🍎🍎'
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"FRUIT","thmLen":57,"thr":31,' +
                '"rows":"7*.7-.5OO-ZO..ZOO-ZZ.OZ3-.I4ZZ","cur":{"t":"Z","c":3,"r":5,"d":"E",' +
                '"p":"dn"},"nx":"O","sc":1000,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.tweet(rowClearingText, 1234567890L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, squareMoves, 555666777L) >> newGameTweet
        1 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.writeGameState('9*.7-.3Z.OO-.I4ZZf1250T1vmuN0O3n33')
        outcome.tweets == [lineClearingTweet, newGameTweet]
        0 * _._
    }

    def "should clear three rows and tweet row-clearing tweet followed by new game poll tweet"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("↔️ Left or Right", 5),
                new Choice("🔄 Rotate", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80) // Super majority (over 70)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽◽◽◽◽　◽🌸\n' +
                '◽◽◽◽◽◽◽　🌸🌸\n' +
                '🌻◽🌺🌺◽◽◽　🌸\n' +
                '🌻🌻◽🌺◽◽◽\n' +
                '🌻🌻◽🌺◽◽◽\n' +
                '🌻🌻◽◽🍄🍄🍄　Score\n' +
                '🌸🌻🍄◽🌸🍄🍄　100\n' +
                '🌸🌸🍄◽🌸🌸🌸\n' +
                '◽🌻🌻🌻🌻🌸🌸'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '🌻◽◽◽◽◽◽\n' +
                '🌻🌻◽◽◽◽◽\n' +
                '🌻🌻◽◽◽◽◽\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+175\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+175\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+175\n' +
                '◽🌻🌻🌻🌻🌸🌸'
        String newGameText = '' +
                '◽◽🌸🌸◽◽◽\n' +
                '◽◽◽🌸🌸◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽　🌹🌹\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '🌻◽◽◽◽◽◽　Score\n' +
                '🌻🌻◽◽◽◽◽　625\n' +
                '🌻🌻◽◽◽◽◽\n' +
                '◽🌻🌻🌻🌻🌸🌸'
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"PLANT","thmLen":57,"thr":31,' +
                '"rows":"4*.7-I.6-2*II.5-II..S3-ZIS.ZSS-ZZS.ZZZ-.I4ZZ","cur":{"t":"L","c":3,' +
                '"r":5,"d":"E","p":"dn"},"nx":"Z","sc":100,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.tweet(rowClearingText, 1234567890L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, startingMoves, 555666777L) >> newGameTweet
        1 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.writeGameState('7*.7-I.6-2*II.5-.I4ZZp625T1vmuN0Z3n33')
        outcome.tweets == [lineClearingTweet, newGameTweet]
        0 * _._
    }

    def "should clear four rows and tweet row-clearing tweet followed by new game poll tweet"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("🔄 Rotate", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80) // Super majority (over 70)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽🌻◽◽◽　Next\n' +
                '◽◽◽🌻◽◽◽　◽🌸\n' +
                '◽◽◽🌻◽◽◽　🌸🌸\n' +
                '🌻◽🌸🌻🍄🍄◽　🌸\n' +
                '🌻🌻🌸◽🍄🌸🌸\n' +
                '🌻🌻🌿◽🌿🌿🌿\n' +
                '🌻🌻🌿◽🍄🍄🍄　Score\n' +
                '🌸🌻🍄◽🌸🍄🍄　100\n' +
                '🌸🌸🍄◽🌸🌸🌸\n' +
                '◽🌻🌻🌻🌻🌸🌸'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '🌻◽🌸◽🍄🍄◽\n' +
                '🌻🌻🌸◽🍄🌸🌸\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+250\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+250\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+250\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+250\n' +
                '◽🌻🌻🌻🌻🌸🌸'
        String newGameText = '' +
                '◽◽🌸🌸◽◽◽\n' +
                '◽◽◽🌸🌸◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽　🌹🌹\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '🌻◽🌸◽🍄🍄◽　1100\n' +
                '🌻🌻🌸◽🍄🌸🌸\n' +
                '◽🌻🌻🌻🌻🌸🌸'
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"PLANT","thmLen":57,"thr":31,' +
                '"rows":"4*.7-I.Z.SS.-IIZ.SZZ-IIO.OOO-IIO.SSS-ZIS.ZSS-ZZS.ZZZ-.I4ZZ","cur":' +
                '{"t":"I","c":3,"r":3,"d":"E","p":"dn"},"nx":"Z","sc":100,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.tweet(rowClearingText, 1234567890L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, startingMoves, 555666777L) >> newGameTweet
        1 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.writeGameState('8*.7-I.Z.SS.-IIZ.SZZ-.I4ZZp1100T1vmuN0Z3n33')
        outcome.tweets == [lineClearingTweet, newGameTweet]
        0 * _._
    }

    def "line clear tweet should show board clear bonus and one line clear"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("↔️ Left or Right", 5),
                new Choice("🔄 Rotate", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80) // Super majority (over 70)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽🌻🌻🌻🌻◽◽　◽🌸\n' +
                '◽◽◽◽◽◽◽　🌸🌸\n' +
                '◽◽◽◽◽◽◽　🌸\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽◽◽◽◽　100\n' +
                '◽◽◽◽◽◽◽\n' +
                '🌸◽◽◽◽🍄🍄'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽　+1000\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+100'
        String newGameText = '' +
                '◽◽🌸🌸◽◽◽\n' +
                '◽◽◽🌸🌸◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽　🌹🌹\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽◽◽◽◽　1200\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽'
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"PLANT","thmLen":57,"thr":31,' +
                '"rows":"9*.7-.7-Z.4SS","cur":' +
                '{"t":"I","c":2,"r":2,"d":"N","p":"dn"},"nx":"Z","sc":100,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.tweet(rowClearingText, 1234567890L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, startingMoves, 555666777L) >> newGameTweet
        1 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.writeGameState('9*.7-2*.7p1200T1vmuN0Z3n33')
        outcome.tweets == [lineClearingTweet, newGameTweet]
        0 * _._
    }

    def "line clear tweet should show board clear bonus and two line clears"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("↔️ Left or Right", 5),
                new Choice("🔄 Rotate", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80) // Super majority (over 70)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽🍀🍀🍀◽◽　◽🌸\n' +
                '◽◽◽◽🍀◽◽　🌸🌸\n' +
                '◽◽◽◽◽◽◽　🌸\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽◽◽◽◽　100\n' +
                '🌸🌻◽◽◽🍄🍄\n' +
                '🌸🌸🍄🌸◽🌸🌸'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽　+1000\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+125\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+125'
        String newGameText = '' +
                '◽◽🌸🌸◽◽◽\n' +
                '◽◽◽🌸🌸◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽　🌹🌹\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽◽◽◽◽　1350\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽'
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"PLANT","thmLen":57,"thr":31,' +
                '"rows":"9*.7-ZI...SS-ZZSZ.ZZ","cur":' +
                '{"t":"J","c":3,"r":2,"d":"N","p":"dn"},"nx":"Z","sc":100,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.tweet(rowClearingText, 1234567890L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, startingMoves, 555666777L) >> newGameTweet
        1 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.writeGameState('9*.7-2*.7p1350T1vmuN0Z3n33')
        outcome.tweets == [lineClearingTweet, newGameTweet]
        0 * _._
    }

    def "line clear tweet should show board clear bonus and three line clears"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("↔️ Left or Right", 5),
                new Choice("🔄 Rotate", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80) // Super majority (over 70)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Next\n' +
                '◽◽◽🍀🍀◽◽　◽🌸\n' +
                '◽◽◽🍀◽◽◽　🌸🌸\n' +
                '◽◽◽🍀◽◽◽　🌸\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '🌻🌻🌿◽◽🍄🍄　100\n' +
                '🌸🌻🍄◽🌸🍄🍄\n' +
                '🌸🌸🍄◽🌸🌸🌸'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽　+1000\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+175\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+175\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+175'
        String newGameText = '' +
                '◽◽🌸🌸◽◽◽\n' +
                '◽◽◽🌸🌸◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽　🌹🌹\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽◽◽◽◽　1625\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽'
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"PLANT","thmLen":57,"thr":31,' +
                '"rows":"8*.7-IIO..SS-ZIS.ZSS-ZZS.ZZZ","cur":' +
                '{"t":"J","c":4,"r":3,"d":"W","p":"dn"},"nx":"Z","sc":100,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.tweet(rowClearingText, 1234567890L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, startingMoves, 555666777L) >> newGameTweet
        1 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.writeGameState('9*.7-2*.7p1625T1vmuN0Z3n33')
        outcome.tweets == [lineClearingTweet, newGameTweet]
        0 * _._
    }

    def "line clear tweet should show board clear bonus and four line clears"() {
        setup:
        initEnvironment()
        Database database = Mock()
        TwitterPollMaker pollMaker = Mock()
        Random random = new Random(4)
        Time time = Mock()
        Player player = new Player(random, time, database, pollMaker)
        StatusWithCard previousTweet = Mock()
        Card previousPoll = new FakeCard()
        previousPoll.choices = [
                new Choice("↔️ Left or Right", 5),
                new Choice("🔄 Rotate", 5),
                new Choice("⬇️ Down", 10),
                new Choice("⏬ Plummet", 80) // Super majority (over 70)
        ].toArray(new Choice[0])
        previousPoll.countsAreFinal = false
        String previousGameText = '' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽🌻◽◽◽　Next\n' +
                '◽◽◽🌻◽◽◽　◽🌸\n' +
                '◽◽◽🌻◽◽◽　🌸🌸\n' +
                '◽◽◽🌻◽◽◽　🌸\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '🌻🌻🌿◽🌿🌿🌿　Score\n' +
                '🌻🌻🌿◽🍄🍄🍄　100\n' +
                '🌸🌻🍄◽🌸🍄🍄\n' +
                '🌸🌸🍄◽🌸🌸🌸'
        String rowClearingText = '' +
                '◽◽◽◽◽◽◽　+1000\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+250\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+250\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+250\n' +
                '🍂🍂🍂🍂🍂🍂🍂　+250'
        String newGameText = '' +
                '◽◽🌸🌸◽◽◽\n' +
                '◽◽◽🌸🌸◽◽　Next\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽　🌹🌹\n' +
                '◽◽◽◽◽◽◽　🌹\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽　Score\n' +
                '◽◽◽◽◽◽◽　2100\n' +
                '◽◽◽◽◽◽◽\n' +
                '◽◽◽◽◽◽◽'
        Status lineClearingTweet = Mock()
        Status newGameTweet = Mock()

        when:
        Outcome outcome = player.play()

        then:
        outcome.getPreviousGameString() == previousGameText
        1 * database.readGameState() >> '{"thm":"PLANT","thmLen":57,"thr":31,' +
                '"rows":"7*.7-IIO.OOO-IIO.SSS-ZIS.ZSS-ZZS.ZZZ","cur":' +
                '{"t":"I","c":3,"r":3,"d":"E","p":"dn"},"nx":"Z","sc":100,"id":1234567890}'
        1 * pollMaker.readPreviousTweet(1234567890L) >> previousTweet
        2 * previousTweet.getCard() >> previousPoll
        1 * previousTweet.getId() >> 1234567890L
        1 * pollMaker.tweet(rowClearingText, 1234567890L, null) >> lineClearingTweet
        1 * lineClearingTweet.getId() >> 555666777L
        1 * pollMaker.postPoll(20, newGameText, startingMoves, 555666777L) >> newGameTweet
        1 * newGameTweet.getId() >> 87654L
        1 * time.nowZonedDateTime() >> ZonedDateTime.parse('2018-04-19T06:34:55Z')
        1 * database.writeGameState('9*.7-2*.7p2100T1vmuN0Z3n33')
        outcome.tweets == [lineClearingTweet, newGameTweet]
        0 * _._
    }
}
